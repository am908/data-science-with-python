from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.metrics import balanced_accuracy_score , f1_score ,  average_precision_score
from imblearn.metrics import geometric_mean_score
from collections import Counter


def confusion(classifier, X_test, y_test):
    y_pred  = classifier.predict(X_test)
    
    cm = confusion_matrix(y_test, y_pred, normalize='all')
    cmd = ConfusionMatrixDisplay(cm, display_labels=['0','1'])
    cmd.plot()

    return confusion_matrix(y_test, y_pred).ravel()

# get the result of cofusion matrix
def show(tn,fp,fn,tp):
    print("TN:" + str(tn) + " FP:" + str(fp) + " FN:" + str(fn) + " TP:" + str(tp) + 
          " FNR=" + str(fn/(fn+tp)) + " FPR=" + str(fp/(fp+tn)))

# Draw histogram for label column
def bar_func(df_class):
    count_classes = pd.value_counts(df_class, sort = True)
    count_classes.plot(kind = 'bar')
    plt.title("Fraud class histogram")
    plt.xlabel("Class")
    plt.ylabel("Frequency")
    plt.xticks(rotation=0)

# Draw Scatter plot for the data to compare between the two classes by time and ammount, to see the defferance of classes distribution and balancing.
def Scatter_plot(df,label):
    Counter_label = Counter(label)
    first_class  = plt.scatter(df[label==0]['Time'],df[label==0]["Amount"], color='brown')
    second_class = plt.scatter(df[label==1]['Time'],df[label==1]["Amount"], color="gray")
    plt.xlabel("Time Column")
    plt.ylabel("Amount Column")
    plt.title("Scatter Plot for Fruad data")
    plt.legend((first_class,second_class), ('Class0:'+str(Counter_label[0]), 'Class1:'+str(Counter_label[1])),scatterpoints=1,loc='upper left',ncol=3,fontsize=8)
    plt.show()
    
# calculate accuracy
def getAccuracyMeasures(y_test, y_pred):
    acuuracy_list = [balanced_accuracy_score(y_test, y_pred),
                     f1_score(y_test, y_pred),
                     geometric_mean_score(y_test, y_pred), 
                     average_precision_score(y_test, y_pred)
                    ]
    return acuuracy_list



# function to add value labels to bar chart
def addlabels(x,y):
    for i in range(len(x)):
        plt.text(i, y[i],y[i], ha = 'center',bbox = dict(facecolor = 'blue', alpha = .5))
def barPlotChart(x,y,model_name):
    # setting figure size by using figure() function
    plt.figure(figsize = (10,5))
    # making the bar chart on the data with color red
    plt.bar(x, y, color = 'gray')
    # calling the function to add value labels
    addlabels(x, y)
    # giving title to the plot
    plt.title("Accuracy Meatures For '"+model_name+"'")
    # giving X and Y labels
    plt.xlabel("Measures")
    plt.ylabel("Accuracy")
    # visualizing the plot
    plt.show()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 